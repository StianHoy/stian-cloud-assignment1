package main

import (
	"encoding/json"
	"fmt"
	"html"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)


type Country struct {
	Code string `json:"alpha2Code"`
	CountryName string `json:"name"`
	CountryFlag string `json:"flag"`
	SpeciesArray []string `json:"species"`
	SpeciesKeyArray []int `json:"speciesKey"`
	

}



type Species struct {
	Key int `json:"key"`
	Kingdom string `json:"kingdom"`
	Phylum string `json:"phylum"`
	Order string `json:"order"`
	Family string `json:"family"`
	Genus string `json:"genus"`
	ScientificName string `json:"scientificName"`
	CanonicalName string `json:"canonicalName"`
	Year string `json:"year"`
}

type speciesYear struct {
	Year string `json:"year"`
	BracketYear string `json:"bracketYear"`
}

type Diagnostic struct {

	StatusGBIF int `json:"gbif"`
	RestCountry int `json:"restcountries"`
	Version string `json:"version"`
	Uptime int64 `json:"uptime"`
}

var startTime = time.Now().Unix()

type countriesArray struct {
	Results []countriesInfo `json:"results"`
}

type countriesInfo struct {
	Species string `json:"species"`
	SpeciesKey int `json:"speciesKey"`
}



func defaultHandler (w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, this is the main page. To view other pages pleace hit conservation/v1/country/species or diag%q", html.EscapeString(r.URL.Path))
}



func countryHandler (w http.ResponseWriter, r *http.Request) {

	countryCode := r.URL.Path[25:]
	//If there is no speciesKey entered we return an error
	if len(countryCode) != 2 {
		http.Error(w, "Wrong country code used", http.StatusBadRequest)
	}
	//Get on the speciesKey entered by the user
	resp, err := http.Get(fmt.Sprintf("https://restcountries.eu/rest/v2/alpha/%s", countryCode))
	if err != nil {
		log.Fatalln(err)
	}
	// _ is used because we don't care about the error code returned by the function
	var countryData Country
	_ = json.NewDecoder(resp.Body).Decode(&countryData)


	limit := 20
	if r.URL.Query()["limit"] != nil {
		customLimit := r.URL.Query()["limit"][0]
		customLimitInt, err := strconv.Atoi(customLimit)
		if err == nil {
			limit = customLimitInt
		}
	}


	offset := 0
	speciesMap := make(map[int]string)
	if limit > 300 {
		for ; limit > 300 ; {


			resp, err := http.Get(fmt.Sprintf("http://api.gbif.org/v1/occurrence/search?country=%s&limit=%d&offset=%d", countryCode, limit, offset))
			if err != nil {
				log.Fatalln(err)
			}

			var countryArray countriesArray
			_ = json.NewDecoder(resp.Body).Decode(&countryArray)


			for i := 0 ;i < len(countryArray.Results) ; i++ {
				speciesMap[countryArray.Results[i].SpeciesKey] = countryArray.Results[i].Species
			}


			offset += 300
			limit -= 300
		}

	} else {
		resp, err := http.Get(fmt.Sprintf("http://api.gbif.org/v1/occurrence/search?country=%s&limit=%d&offset=%d", countryCode, limit, offset))
		if err != nil {
			log.Fatalln(err)
		}

		var countryArray countriesArray
		_ = json.NewDecoder(resp.Body).Decode(&countryArray)


		for i := 0 ;i < len(countryArray.Results) ; i++ {
			speciesMap[countryArray.Results[i].SpeciesKey] = countryArray.Results[i].Species
		}
	}

	//Maps the values from the speciesMap to the arrays found in countryData struct.
	for i, j := range speciesMap {
		countryData.SpeciesKeyArray = append(countryData.SpeciesKeyArray, i)
		countryData.SpeciesArray = append(countryData.SpeciesArray, j)
	}

	//Prints the information to the website
	w.Header().Add("content-type", "application/json")
	_ = json.NewEncoder(w).Encode(countryData)

}



func speciesHandler (w http.ResponseWriter, r *http.Request) {


	//Gets the information in the URL that contains the speciesKey the users wants to look up
	speciesKey := r.URL.Path[25:]
	//If there is no speciesKey entered we return an error
	if len(speciesKey) == 0 {
		http.Error(w, "No species key found", http.StatusBadRequest)
	}
	//Get on the speciesKey entered by the user
	resp, err := http.Get(fmt.Sprintf("http://api.gbif.org/v1/species/%s", speciesKey))
	if err != nil {
		log.Fatalln(err)
	}
	// _ is used because we don't care about the error code returned by the function
	var speciesData Species
	_ = json.NewDecoder(resp.Body).Decode(&speciesData)

	resp, err2 := http.Get(fmt.Sprintf("http://api.gbif.org/v1/species/%s/name", speciesKey))
	if err2 != nil {
		log.Fatalln(err2)
	}

	//Enters the species name page and gets information about the year.
	//The year can be in two formats, year or bracketYear.
	var speciesYear speciesYear
	_ = json.NewDecoder(resp.Body).Decode(&speciesYear)

	//If there is something in the bracketYear we use this, if there is nothing in the bracketYear we use the default year.
	//If this is blank too then there is no year and it remains blank.
	if speciesYear.BracketYear != "" {
		speciesData.Year = speciesYear.BracketYear
	} else {
		speciesData.Year = speciesYear.Year
	}


	w.Header().Add("content-type", "application/json")
	_ = json.NewEncoder(w).Encode(speciesData)

}



func diagHandler (w http.ResponseWriter, r *http.Request) {

	t := time.Now().Unix()

	gbifResp, err := http.Get("https://api.gbif.org/v1/")
	if err != nil {
		log.Fatalln(err)
	}
	gbifStatus := gbifResp.StatusCode

	restResp, err := http.Get("https://restcountries.eu/rest/v2/alpha/KZ")
	if err != nil {
		log.Fatalln(err)
	}
	restCountryStatus := restResp.StatusCode


	diagData := Diagnostic{gbifStatus, restCountryStatus, "v1", t - startTime }


	w.Header().Add("content-type", "application/json")
	_ = json.NewEncoder(w).Encode(diagData)


}

func main () {


	port := os.Getenv("PORT")

	if port == "" {
		//log.Fatal("$PORT must be set")
		port = "8080"
	}

	http.HandleFunc("/", defaultHandler)
	http.HandleFunc("/conservation/v1/country/" , countryHandler)
	http.HandleFunc("/conservation/v1/species/", speciesHandler)
	http.HandleFunc("/conservation/v1/diag/", diagHandler)
	log.Fatal(http.ListenAndServe(":"+port, nil))

}
